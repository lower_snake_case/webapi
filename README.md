# flaschenpost SE – IT Exercise Task #5

# „C# Restful API Service to analyze JSON product data“ 



## Table of content

1. [Exercise](#exersice)
   - [Requirements](#requirements)
   - [Get all data](#getting-all-data-from-given-url)
   - [Task #1](#task-1)
   - [Task #2](#task-2)
   - [Task #3](#task-3)
   - [All tasks together](#all-tasks-together)
2. [Alternative downloads](#alternative-download)
   - [Release](#release)
   - [Debug](#debug)

## Exercise



### Requirements

The exercise requires a generic C#/.NET Restful API which can easily be compiled on any typical developer's environment.

Choosing routes for each task was up to me but there is the following example in the definition:

> A route may look like this: /api/mostBottles?url=http://urlto/productData.json 



### Getting all from the given URL

Please use the following route the get all the data of the source JSON file (*you'll have to replace the `PORT`*):

`api/product?url=https://flapotest.blob.core.windows.net/test/ProductData.json`



### Task #1

> Most expensive and cheapest beer per litre.

use `api/product/cheapest-and-most-expensive-beer-per-litre?url=https://flapotest.blob.core.windows.net/test/ProductData.json` route to get the most expensive beers and the cheapest beers.



### Task #2

If you would like to get all beers which cost a exact price use `api/product/exact-price?url=https://flapotest.blob.core.windows.net/test/ProductData.json` or optional with `&price=X.XX` to not use the default price of €17.99.

**The resulting list is order by price per litre *ascending***



### Task #3

#### Question

> Which one product comes in the most bottles?



#### Route

`api/product/most-bottles?url=https://flapotest.blob.core.windows.net/test/ProductData.json`



### All tasks together

#### Instruction

> It also has one route to get the answer to all routes or questions at once. 



#### Route

`http://localhost:50862?url=https://flapotest.blob.core.windows.net/test/ProductData.json`



## Alternative downloads

### [Release](https://mega.nz/folder/NH5SFARb#IikdqyKGfBzKimFaQw1sPQ)

### [Debug](https://mega.nz/folder/gKo0TKhC#s_Wi0ltthua5GaecgSk8rw)

